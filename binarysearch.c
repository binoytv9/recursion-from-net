#include<stdio.h>

int bsearch(int a[],int n,int l,int h);
void asort(int a[],int l);

main()
{
	int l,i,a[100],m,n;

	printf("\nhow many elements : ");
	scanf("%d",&l);

	for(i=0;i<l;++i){
		printf("\n\ta[%d] : ",i);
		scanf("%d",&a[i]);
	}

	printf("\nnumer to search : ");
	scanf("%d",&n);

	asort(a,l);

	if((m=bsearch(a,n,0,l-1))>=0)
		printf("\n%d found at position %d\n\n",n,m+1);
	else
		printf("\nnot found!!!\n\n");
}

int bsearch(int a[],int n,int l,int h)
{
	int m;

	if(l<=h){
		m=(l+h)/2;
		if(a[m]==n)
			return m;
		else if(n<a[m])
			return bsearch(a,n,l,m-1);
		else
			return bsearch(a,n,m+1,h);
	}
	else
		return -1;
}
			

void asort(int a[],int l)
{
	int i,j,t;

	for(i=0;i<l;++i)
		for(j=1;j<l-i;++j)
			if(a[j]<a[j-1]){
				t=a[j];
				a[j]=a[j-1];
				a[j-1]=t;
			}
}
