/*	The greatest common divisor (g.c.d.) of two nonnegative integers is the largest
 *	integer that divides evenly into both. In the third century B . C ., the Greek
 *	mathematician Euclid discovered that the greatest common divisor of x and y can
 *	always be computed as follows:
 *		• If x is evenly divisible by y, then y is the greatest common divisor.
 *		• Otherwise, the greatest common divisor of x and y is always equal to the greatest
 *		  common divisor of y and the remainder of x divided by y.
 *	Use Euclid’s insight to write a recursive function GCD(x, y) that computes the
 *	greatest common divisor of x and y.
 */

#include<stdio.h>

int gcd(int x,int y);

main()
{
	int x,y,t;

	printf("\nentr x: ");
	scanf("%d",&x);
	
	printf("\nentr y: ");
	scanf("%d",&y);

	if(y>x){
		t=x;
		x=y;
		y=t;
	}

	printf("\n\n\tgreatest common divisor of %d and %d is %d\n\n",x,y,gcd(x,y));
}

int gcd(int x,int y)
{
	if(x%y==0)
		return y;
	else
		return gcd(y,x%y);
}
