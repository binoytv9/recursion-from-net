/*	Spherical objects, such as cannonballs, can be stacked to form a pyramid with one
 *	cannonball at the top, sitting on top of a square composed of four cannonballs, sitting
 *	on top of a square composed of nine cannonballs, and so forth. Write a recursive
 *	function Cannonball that takes as its argument the height of the pyramid and returns
 *	the number of cannonballs it contains.
 */

#include<stdio.h>

long cannonball(int h);

main()
{
	int h;

	printf("\nenter the height of pyramid : ");
	scanf("%d",&h);

	printf("\n\n\tno of cannon balls required is %ld\n\n",cannonball(h));
}

long cannonball(int h)
{
	if(h>1)
		return h*h + cannonball(h-1);

	else
		return 1;
}
