/*	Write a recursive function DigitSum(n) that takes a nonnegative integer and returns
 *	the sum of its digits. For example, calling DigitSum(1729) should return
 *	1 + 7 + 2 + 9, which is 19.
 */

#include<stdio.h>

long digitsum(long n);

main()
{
	long n;

	printf("\nentr the number : ");
	scanf("%ld",&n);

	printf("\n\n\tsum of digits of %ld is %ld\n\n",n,digitsum(n));
}

long digitsum(long n)
{
	if(n)
		return n%10 + digitsum(n/10);

	else
		return 0;
}
