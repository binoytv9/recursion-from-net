/*	Write a recursive function that takes a string as argument and returns the reverse of
 *	that string.
 */

#include<stdio.h>

#define ML 100

char *reverse(char *s);

char buf[100],*p=buf;

main()
{
	char str[ML];

	printf("\nentr the string : ");
	scanf("%s",str);

	printf("\n\n\treverse of '%s' is %s\n\n",str,reverse(str));
}

char *reverse(char *s)
{
	if(*s){
		reverse(s+1);
		*p++ = *s;
	}

	return buf;
}
