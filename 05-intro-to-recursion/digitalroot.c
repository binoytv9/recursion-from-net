/*	The digital root of an integer n is defined as the result of summing the digits
 *	repeatedly until only a single digit remains. For example, the digital root of 1729
 *	can be calculated using the following steps:
 *		Step 1: 1 + 7 + 2 + 9	->	19
 *		Step 2: 1 + 9		->	10
 *		Step 3: 1 + 0		->	1
 *	Because the total at the end of step 3 is the single digit 1, that value is the digital root.
 */

#include<stdio.h>

long digitalroot(long n);
long digitsum(long n);

main()
{
	long n;

	printf("\nentr the number : ");
	scanf("%ld",&n);

	printf("\n\n\tdigital root of %ld is %ld\n\n",n,digitalroot(n));
}

long digitsum(long n)
{
	if(n)
		return n%10 + digitsum(n/10);
	else
		return 0;
}

long digitalroot(long n)
{
	if(n>9)
		return digitalroot(digitsum(n));
	else
		return n;
}
