/*	program for finding combination using the Pascal's triangle
 */

#include<stdio.h>

long comb(int n,int k);

main()
{
	int n,k;

	printf("\nentr n : ");
	scanf("%d",&n);

	printf("\nentr k : ");
	scanf("%d",&k);

	printf("\n\n\tC(%d,%d) is %ld\n\n",n,k,comb(n,k));
}

long comb(int n,int k)
{
	if(k==0 || k==n)
		return 1;
	else
		return comb(n-1,k-1) + comb(n-1,k);
}
