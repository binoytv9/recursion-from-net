/*	write the IntegerToString implementation so that it operates recursively without
 *	using use any of the iterative constructs such as while and for.
 */

#include<stdio.h>

char *int2str(long n);

char str[50],*s=str;

main()
{
	long n;

	printf("\nentr the number : ");
	scanf("%ld",&n);

	printf("\n\nstring equivalent of %ld is %s\n\n",n,int2str(n));
}

char *int2str(long n)
{
	if(n){
		int2str(n/10);
		*s++ = n%10 + '0';
	}

	return str;
}
