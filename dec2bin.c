#include<stdio.h>

long dec2bin(int dec);

main()
{
	int dec;

	printf("\nentr the decimal number to be converted : ");
	scanf("%d",&dec);

	printf("\nbinary equivalent of %d is %ld\n\n",dec,dec2bin(dec));
}

long dec2bin(int dec)
{
	if(dec)
		return  dec2bin(dec/2)*10 + dec%2;
	else
		return 0;
}
