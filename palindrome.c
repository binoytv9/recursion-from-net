#include<stdio.h>
#include<string.h>

#define ML 500

int palindrome(char *s,int l,int r);

main()
{
	char str[ML];
	int l;

	printf("\nentr the number/string : ");
	scanf("%s",str);

	l=strlen(str);

	printf("\n\t%s",str);
	if(palindrome(str,0,l-1))
		printf(" is palindrome!!!\n\n");
	else
		printf(" is not palindrome!!!\n\n");
}

int palindrome(char *s,int l,int r)
{
	if(l<r){
		if(s[l]==s[r])
			return palindrome(s,++l,--r);
		else
			return 0;
	}
	else
		return 1;
}
