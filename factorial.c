#include<stdio.h>

long fact(int n);

main()
{
	int n;

	printf("\nentr the number : ");
	scanf("%d",&n);

	if(n>=0)
		printf("\nfactorial of %d is %ld\n\n",n,fact(n));
	else
		printf("\nfactorial of %d (negative number) is undefined !!!\n\n",n);
}

long fact(int n)
{
	if(n)
		return n*fact(n-1);
	else
		return 1;
}
