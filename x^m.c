#include<stdio.h>

long power(int x,int m);

main()
{
	int x,m;

	printf("\nentr value for x : ");
	scanf("%d",&x);
	
	printf("\nentr value for m : ");
	scanf("%d",&m);

	printf("\n\n%d^%d is %ld\n\n",x,m,power(x,m));
}

long power(int x,int m)
{
	if(m>0)
		return x*power(x,m-1);
	else
		return 1;
}
